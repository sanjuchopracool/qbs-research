#include <iostream>
#include <iomanip>
#include "NRF24L01P_host.h"

using namespace std;


int main()
{
    NRF24L01P_Host theRf;
    std::array<unsigned char, 14> data;
    data[0] = 0x08;
    data[1] = 0x02;
    data[2] = 0x0F;
    data[3] = 0x20;
    data[4] = 0xE7;
    data[5] = 0xE7;
    data[6] = 0xE7;
    data[7] = 0xE7;
    data[8] = 0xE7;
    data[9] = 0xE7;
    data[10] = 0xE7;
    data[11] = 0xE7;
    data[12] = 0xE7;
    data[13] = 0xE7;

    theRf.LoadFromBytes( data );
    cout << "Air Data Rate: " << (int)theRf.GetAirDataRate() << endl;
    cout << "Rf Output Power: " << (int)theRf.GetRfOutputPower() << endl;
    cout << "Rf Frequency: " << theRf.GetRfFrequency() << endl;
    cout << "CRC Width: " << (int)theRf.GetCrcWidth() << endl;
    cout << "Is Powered Up: " << boolalpha << theRf.IsPoweredUp() << endl;
    cout << "Transfer Size: " << (int)theRf.GetTransferSize() << endl;
    cout << "TX Address: " << hex << theRf.GetTxAddress() << endl;
    cout << "RX Address: " << hex << theRf.GetRxAddress() << endl;

    cout << "Checking serialisation " << boolalpha << (data == theRf.ToBytes()) << endl;
    return 0;
}
