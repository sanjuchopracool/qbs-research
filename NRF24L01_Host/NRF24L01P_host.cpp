#include "NRF24L01P_host.h"
// CONFIG register:
#define _NRF24L01P_CONFIG_PRIM_RX        (1<<0)
#define _NRF24L01P_CONFIG_PWR_UP         (1<<1)
#define _NRF24L01P_CONFIG_CRC0           (1<<2)
#define _NRF24L01P_CONFIG_EN_CRC         (1<<3)
#define _NRF24L01P_CONFIG_MASK_MAX_RT    (1<<4)
#define _NRF24L01P_CONFIG_MASK_TX_DS     (1<<5)
#define _NRF24L01P_CONFIG_MASK_RX_DR     (1<<6)

#define _NRF24L01P_CONFIG_CRC_MASK       (_NRF24L01P_CONFIG_EN_CRC|_NRF24L01P_CONFIG_CRC0)
#define _NRF24L01P_CONFIG_CRC_NONE       (0)
#define _NRF24L01P_CONFIG_CRC_8BIT       (_NRF24L01P_CONFIG_EN_CRC)
#define _NRF24L01P_CONFIG_CRC_16BIT      (_NRF24L01P_CONFIG_EN_CRC|_NRF24L01P_CONFIG_CRC0)

//RF_CH
#define NRF24L01P_MIN_RF_FREQUENCY    2400
#define NRF24L01P_MAX_RF_FREQUENCY    2525

// RF_SETUP register:
#define _NRF24L01P_RF_SETUP_RF_PWR_MASK          (0x3<<1)
#define _NRF24L01P_RF_SETUP_RF_PWR_0DBM          (0x3<<1)
#define _NRF24L01P_RF_SETUP_RF_PWR_MINUS_6DBM    (0x2<<1)
#define _NRF24L01P_RF_SETUP_RF_PWR_MINUS_12DBM   (0x1<<1)
#define _NRF24L01P_RF_SETUP_RF_PWR_MINUS_18DBM   (0x0<<1)

#define _NRF24L01P_RF_SETUP_RF_DR_HIGH_BIT       (1 << 3)
#define _NRF24L01P_RF_SETUP_RF_DR_LOW_BIT        (1 << 5)
#define _NRF24L01P_RF_SETUP_RF_DR_MASK           (_NRF24L01P_RF_SETUP_RF_DR_LOW_BIT|_NRF24L01P_RF_SETUP_RF_DR_HIGH_BIT)
#define _NRF24L01P_RF_SETUP_RF_DR_250KBPS        (_NRF24L01P_RF_SETUP_RF_DR_LOW_BIT)
#define _NRF24L01P_RF_SETUP_RF_DR_1MBPS          (0)
#define _NRF24L01P_RF_SETUP_RF_DR_2MBPS          (_NRF24L01P_RF_SETUP_RF_DR_HIGH_BIT)

NRF24L01P_Host::NRF24L01P_Host()
{

}

bool NRF24L01P_Host::SetAirDataRate(NRF24L01P_Host::AirDataRate inDataRate)
{
    uint8_t theRFSetup = _RF_SETUP & ~_NRF24L01P_RF_SETUP_RF_DR_MASK;
    switch ( inDataRate )
    {
    case AirDataRate::DR_250_KBPS:
        theRFSetup |= _NRF24L01P_RF_SETUP_RF_DR_250KBPS;
        break;
    case AirDataRate::DR_1_MBPS:
        theRFSetup |= _NRF24L01P_RF_SETUP_RF_DR_1MBPS;
        break;
    case AirDataRate::DR_2_MBPS:
        theRFSetup |= _NRF24L01P_RF_SETUP_RF_DR_2MBPS;
        break;
    default:
        return false;
    }

    _RF_SETUP = theRFSetup;
    return true;
}

NRF24L01P_Host::AirDataRate NRF24L01P_Host::GetAirDataRate() const
{
    uint8_t theRFSetup = _RF_SETUP & _NRF24L01P_RF_SETUP_RF_DR_MASK;
    switch ( theRFSetup )
    {
    case _NRF24L01P_RF_SETUP_RF_DR_250KBPS:
        return AirDataRate::DR_250_KBPS;

    case _NRF24L01P_RF_SETUP_RF_DR_1MBPS:
        return AirDataRate::DR_1_MBPS;

    case _NRF24L01P_RF_SETUP_RF_DR_2MBPS:
        return AirDataRate::DR_2_MBPS;

    default:
        return AirDataRate::DR_INVALID;
    }
}

bool NRF24L01P_Host::SetRfOutputPower(NRF24L01P_Host::TX_PWR inPwr)
{
    uint8_t theRFSetup = _RF_SETUP & ~_NRF24L01P_RF_SETUP_RF_PWR_MASK;
    switch ( inPwr )
    {
    case TX_PWR::ZERO_DB:
        theRFSetup |= _NRF24L01P_RF_SETUP_RF_PWR_0DBM;
        break;
    case TX_PWR::MINUS_6_DB:
        theRFSetup |= _NRF24L01P_RF_SETUP_RF_PWR_MINUS_6DBM;
        break;
    case TX_PWR::MINUS_12_DB:
        theRFSetup |= _NRF24L01P_RF_SETUP_RF_PWR_MINUS_12DBM;
        break;
    case TX_PWR::MINUS_18_DB:
        theRFSetup |= _NRF24L01P_RF_SETUP_RF_PWR_MINUS_18DBM;
        break;
    default:
        return false;
    }

    _RF_SETUP = theRFSetup;
    return true;
}

NRF24L01P_Host::TX_PWR NRF24L01P_Host::GetRfOutputPower() const
{
    uint8_t theRFSetup = _RF_SETUP & _NRF24L01P_RF_SETUP_RF_PWR_MASK;

    switch ( theRFSetup )
    {
    case _NRF24L01P_RF_SETUP_RF_PWR_0DBM:
        return TX_PWR::ZERO_DB;

    case _NRF24L01P_RF_SETUP_RF_PWR_MINUS_6DBM:
        return TX_PWR::MINUS_6_DB;

    case _NRF24L01P_RF_SETUP_RF_PWR_MINUS_12DBM:
        return TX_PWR::MINUS_12_DB;

    case _NRF24L01P_RF_SETUP_RF_PWR_MINUS_18DBM:
        return TX_PWR::MINUS_18_DB;

    default:
        return TX_PWR::TX_PWR_INVALID;
    }
}

bool NRF24L01P_Host::SetRfFrequency(uint16_t inFrequency)
{
    if ( ( inFrequency < NRF24L01P_MIN_RF_FREQUENCY ) || ( inFrequency > NRF24L01P_MAX_RF_FREQUENCY ) ) {
        return false;
    }
    uint8_t channel = ( inFrequency - NRF24L01P_MIN_RF_FREQUENCY ) & 0x7F;
    _RF_CH = channel;
    return true;
}

uint16_t NRF24L01P_Host::GetRfFrequency() const
{
    uint16_t channel = _RF_CH & 0x7F;
    return ( channel + NRF24L01P_MIN_RF_FREQUENCY );
}

bool NRF24L01P_Host::SetCrcWidth(NRF24L01P_Host::CRCWidth inCRCWidth)
{
    uint8_t config = _CONFIG & ~_NRF24L01P_CONFIG_CRC_MASK;

    switch ( inCRCWidth )
    {
    case CRCWidth::CRC_NONE:
        config |= _NRF24L01P_CONFIG_CRC_NONE;
        break;
    case CRCWidth::CRC_8_BIT:
        config |= _NRF24L01P_CONFIG_CRC_8BIT;
        break;
    case CRCWidth::CRC_16_BIT:
        config |= _NRF24L01P_CONFIG_CRC_16BIT;
        break;
    default:
        return false;
    }
    _CONFIG = config;
    return true;
}

NRF24L01P_Host::CRCWidth NRF24L01P_Host::GetCrcWidth() const
{
    uint8_t theConfig = _CONFIG & _NRF24L01P_CONFIG_CRC_MASK;

    switch ( theConfig )
    {
    case _NRF24L01P_CONFIG_CRC_NONE:
        return CRCWidth::CRC_NONE;
    case _NRF24L01P_CONFIG_CRC_8BIT:
        return CRCWidth::CRC_8_BIT;
    case _NRF24L01P_CONFIG_CRC_16BIT:
        return CRCWidth::CRC_16_BIT;
    default:
        return CRCWidth::CRC_INVALID;
    }
}

void NRF24L01P_Host::SetPowerUp(bool inUp)
{
    if ( inUp )
    {
        _CONFIG |= _NRF24L01P_CONFIG_PWR_UP;
    }
    else
    {
        _CONFIG &= ~_NRF24L01P_CONFIG_PWR_UP;
    }
}

bool NRF24L01P_Host::IsPoweredUp() const
{
    return ( _CONFIG &  _NRF24L01P_CONFIG_PWR_UP );
}

void NRF24L01P_Host::SetTxAddress(unsigned long long inAddress)
{
    _TX_ADDRESS = inAddress;
}

unsigned long long NRF24L01P_Host::GetTxAddress() const
{
    return _TX_ADDRESS;
}

void NRF24L01P_Host::SetRxAddress(unsigned long long inAddress)
{
    _RX_ADDRESS = inAddress;
}

unsigned long long NRF24L01P_Host::GetRxAddress() const
{
    return _RX_ADDRESS;
}

bool NRF24L01P_Host::LoadFromBytes( const std::array<unsigned char, 14>& inData)
{
    if( inData.size() != 14 )
        return false;

    _CONFIG = inData[0];
    _RF_CH = inData[1];
    _RF_SETUP = inData[2];
    _RX_PW_P0 = inData[3];

    // next 5 bytes tx address
    // LSB first
    _TX_ADDRESS = 0;
    std::size_t txAddressIndex = inData.size() - 10;
    for ( int i = 0; i< 5; i++ ) {
        _TX_ADDRESS |= ( ( (unsigned long long)( inData[txAddressIndex + i] ) ) << (i*8) );
    }
    // next 5 bytes rx address
    // LSB first
    _RX_ADDRESS = 0;
    std::size_t rxAddressIndex = inData.size() - 5;
    for ( int i = 0; i< 5; i++ ) {
        _RX_ADDRESS |= ( ( (unsigned long long)( inData[rxAddressIndex + i] ) ) << (i*8) );
    }

    return true;
}

std::array<unsigned char, 14> NRF24L01P_Host::ToBytes() const
{
    std::array<unsigned char, 14> data;
    data[0] = _CONFIG;
    data[1] = _RF_CH;
    data[2] = _RF_SETUP;
    data[3] = _RX_PW_P0;

    // next 5 bytes tx address
    // LSB first
    unsigned long long txAddress = _TX_ADDRESS;
    std::size_t txAddressIndex = data.size() - 10;
    for ( int i = 0; i< 5; i++ ) {
        data[ txAddressIndex + i ]= (uint8_t) (txAddress & 0xFF);
        txAddress >>= 8;
    }

    // next 5 bytes rx address
    // LSB first
    unsigned long long rxAddress = _RX_ADDRESS;
    std::size_t rxAddressIndex = data.size() - 5;
    for ( int i = 0; i< 5; i++ ) {
        data[ rxAddressIndex + i ]= (uint8_t) (rxAddress & 0xFF);
        rxAddress >>= 8;
    }

    return data;
}

bool NRF24L01P_Host::SetTransferSize(unsigned char inSize)
{
    if( inSize > 32 )
        return false;

    _RX_PW_P0 = inSize;
    return true;
}

unsigned char NRF24L01P_Host::GetTransferSize() const
{
    return _RX_PW_P0;
}
