#ifndef NRF24L01P_HOST_H
#define NRF24L01P_HOST_H

#include <inttypes.h>
#include <array>

class NRF24L01P_Host
{
public:
    NRF24L01P_Host();

    enum class AirDataRate : uint8_t
    {
        DR_250_KBPS = 0,
        DR_1_MBPS,
        DR_2_MBPS,
        DR_INVALID
    };

    bool SetAirDataRate( AirDataRate inDataRate = AirDataRate::DR_250_KBPS );
    AirDataRate GetAirDataRate() const;

    enum class TX_PWR : uint8_t
    {
        ZERO_DB = 0,
        MINUS_6_DB,
        MINUS_12_DB,
        MINUS_18_DB,
        TX_PWR_INVALID
    };

    bool SetRfOutputPower( TX_PWR inPwr = TX_PWR::ZERO_DB );
    TX_PWR GetRfOutputPower() const;

    bool SetRfFrequency( uint16_t inFrequency);
    uint16_t GetRfFrequency () const;

    enum class CRCWidth : uint8_t
    {
        CRC_NONE = 0,
        CRC_8_BIT,
        CRC_16_BIT,
        CRC_INVALID
    };

    bool SetCrcWidth( CRCWidth inCRCWidth = CRCWidth::CRC_NONE );
    CRCWidth GetCrcWidth() const;

    void SetPowerUp( bool inUp );
    bool IsPoweredUp() const;

    void SetTxAddress(unsigned long long inAddress );
    unsigned long long GetTxAddress() const;

    void SetRxAddress(unsigned long long inAddress );
    unsigned long long GetRxAddress() const;

    bool LoadFromBytes( const std::array<unsigned char, 14>& inData );
    std::array<unsigned char, 14> ToBytes() const;

    bool SetTransferSize( unsigned char inSize );
    unsigned char GetTransferSize( ) const;

private:
    uint8_t _CONFIG;
    uint8_t _RF_CH;
    uint8_t _RF_SETUP;
    uint8_t _RX_PW_P0;

    // encode and decode addresses in 5 bytes
    unsigned long long _RX_ADDRESS;
    unsigned long long _TX_ADDRESS;
};

#endif // NRF24L01P_HOST_H
