TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    NRF24L01P_host.cpp

HEADERS += \
    NRF24L01P_host.h
