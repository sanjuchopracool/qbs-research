#include <avr/io.h>

// NOTE: Make sure to adjust F_CPU required for util/delay

#define F_CPU 12000000
#include <util/delay.h>

#include "nrf2401lp_device.h"

void UART_init()
{
    // Formula for noraml asynchronous mode
    // UBRR = F_OSC/(16*BAUD)  -  1

    // F_CPU 12000000
    // BAUD 19200
    // UBRR 38.0625

    UBRRL = 38;  // set prescaler for baud rate
    UCSRB |= 0x08; // enable transmitter
}

void UART_Transmit( unsigned char data )
{
    while ( ! ( UCSRA &  ( 1 << UDRE) ) );
    UDR = data;
}

void printInt( int data )
{
    char buf[5]; // long enough for largest number
    signed char counter = 0;

    if ( data < 0 )
    {
        data *= -1;
        UART_Transmit( '-' );
    }

     if (data == 0)
       buf[counter++] = '0';

     for ( ; data; data /= 10)
     {
         buf[counter++] = '0' + data%10;
     }

     counter --;
     while ( counter >= 0)
         UART_Transmit(buf[counter--]);
}

class SPI
{
public:
    SPI() {
        // Set MOSI and SCK pin as output
        DDRB |= (1<<5)|(1<<3);

        // Master
        // double speed
        // SPI running at F_CPU/2 ( 6MHz)
        // Enable SPE
        SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPI2X);
    }

    unsigned char writeRead( unsigned char data ) {
        SPDR = data;
        while(!(SPSR & (1<<SPIF) ));
        return(SPDR);
    }

    SPI( const SPI& other) = delete;
    SPI& operator =( const SPI& other) = delete;
    SPI& operator =( const SPI&& other) = delete;
    SPI( const SPI&& other) = delete;
};

void delayInSeconds( uint8_t inSeconds)
{
    uint8_t delayCountFor10ms = inSeconds*100;
    for( int i = 0; i <delayCountFor10ms; ++i)
        _delay_ms(10);
}

int main()
{
    UART_init();

    // NRF24l01+ output configuration
    // enable CSN(PB2) as output
    DDRB |= (1<<2);

    // enable CE(PD5) as output
    DDRD |= (1<<5);

    SPI spi;
    NRF24L01P<SPI> theRf(&spi);
    UART_Transmit('*');
    printInt( theRf.GetRegister(0));
    UART_Transmit('\n');
    printInt( theRf.GetRegister(1));
    UART_Transmit('\n');
    printInt( theRf.GetRegister(2));
    UART_Transmit('\n');
    printInt( theRf.GetRegister(3));
    theRf.SetPowerUp( true );
    theRf.SetReceiveMode();
    theRf.SetEnable( true );
    unsigned char data[10];
    while ( 1 )
    {
        if ( theRf.Readable() )
        {
            theRf.Read( data, 10 );
            for ( int i = 0; i < 10; ++i)
                UART_Transmit( data[i] );
            UART_Transmit('\n');
        }
    }
}
