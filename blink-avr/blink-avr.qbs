import qbs 1.0

Product {
    type: "application"
    name: "blink.elf"
    consoleApplication: true

    Depends { name: "cpp" }

    cpp.includePaths: [ "."]
    files: [ "*.h", "*.cpp", "*.txt"]

    cpp.architecture: "avr"


    cpp.positionIndependentCode: false
    cpp.commonCompilerFlags : [ "-g",  "-Os",  "-mmcu=atmega8", "-std=c++11" ]
    cpp.linkerFlags : [ "-g",  "-Os",  "-mmcu=atmega8" ]
    cpp.visibility: "undefined"
    cpp.warningLevel: "none"
    cpp.debugInformation: false
    cpp.enableExceptions : false
    cpp.enableRtti : false
    cpp.allowUnresolvedSymbols: false
    cpp.optimization: "None"
}
