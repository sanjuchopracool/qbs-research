#ifndef SPI_H
#define SPI_H

#include <stdint.h>

class SPIInterface
{
public:
    enum SPIName
    {
        SPI_1,
        SPI_2
    };
    explicit SPIInterface( SPIName inName );
    void Init();
    void Deinit();
    uint8_t WriteRead(uint8_t inByte );

private:
    SPIName mSPI;
};

#endif // SPI_H
