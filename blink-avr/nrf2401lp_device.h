#ifndef NRF24L01P_H
#define NRF24L01P_H

#include "NRF24L01_device_config.h"

#define _NRF24L01P_SPI_CMD_RD_REG            0x00
#define _NRF24L01P_SPI_CMD_WR_REG            0x20
#define _NRF24L01P_SPI_CMD_RD_RX_PAYLOAD     0x61
#define _NRF24L01P_SPI_CMD_WR_TX_PAYLOAD     0xa0
#define _NRF24L01P_SPI_CMD_R_RX_PL_WID       0x60
#define _NRF24L01P_SPI_CMD_FLUSH_RX          0xe2

#define _NRF24L01P_SPI_CMD_NOP               0xff
#define _NRF24L01P_TIMING_Tpece2csn_us       4
#define _NRF24L01P_TIMING_Thce_us              10   //  10uS
#define _NRF24L01P_TIMING_Tpd2stby_us        4500   // 4.5mS worst case

// CONFIG register:
#define _NRF24L01P_REG_CONFIG                0x00
#define _NRF24L01P_CONFIG_PRIM_RX        (1<<0)
#define _NRF24L01P_CONFIG_PWR_UP         (1<<1)

#define _NRF24L01P_REG_EN_AA                 0x01

#define _NRF24L01P_REG_EN_RXADDR             0x02
#define _NRF24L01P_REG_SETUP_RETR            0x04
#define _NRF24L01P_REG_STATUS                0x07
#define _NRF24L01P_REG_RX_PW_P0              0x11

// STATUS register:
#define _NRF24L01P_STATUS_TX_FULL        (1<<0)
#define _NRF24L01P_STATUS_RX_P_NO        (0x7<<1)
#define _NRF24L01P_STATUS_MAX_RT         (1<<4)
#define _NRF24L01P_STATUS_TX_DS          (1<<5)
#define _NRF24L01P_STATUS_RX_DR          (1<<6)

#define _NRF24L01P_TX_FIFO_SIZE   32
#define _NRF24L01P_RX_FIFO_SIZE   32

// EN_AA register:
#define _NRF24L01P_EN_AA_NONE            0

// SETUP_RETR register:
#define _NRF24L01P_SETUP_RETR_NONE       0

template< typename writeReadInterface>
class NRF24L01P
{
public:

    enum class ChipMode : uint8_t
    {
        UNKNOWN,
        POWER_DOWN,
        STANDBY,
        RX,
        TX
    };

    NRF24L01P( writeReadInterface* inDevice ) : mEnabled( false), mSPI( inDevice ), mMode( ChipMode::UNKNOWN) {
        NRF24L01P_RaiseCSN();
        // enable only first rx pipe
        SetRegister( _NRF24L01P_REG_EN_RXADDR, 0x01 );
        // Disable AutoAcknowledgement
        SetRegister(_NRF24L01P_REG_EN_AA, _NRF24L01P_EN_AA_NONE);

        SetRegister(_NRF24L01P_REG_STATUS,
                    _NRF24L01P_STATUS_MAX_RT|
                    _NRF24L01P_STATUS_TX_DS|
                    _NRF24L01P_STATUS_RX_DR);   // Clear any pending interrupts

        // set transfer size
        SetRegister(_NRF24L01P_REG_RX_PW_P0, 10 );
    }

    void SetPowerUp( bool inUp )
    {
        unsigned char theConfig = GetRegister(_NRF24L01P_REG_CONFIG);

        if( inUp )
        {
            theConfig |= _NRF24L01P_CONFIG_PWR_UP;
            mMode = ChipMode::STANDBY;
        }
        else
        {
            theConfig &= ~_NRF24L01P_CONFIG_PWR_UP;
            mMode = ChipMode::POWER_DOWN;
        }

        SetRegister(_NRF24L01P_REG_CONFIG, theConfig);
        // Wait until the nRF24L01+ powers up
        NRF2401LP_DelayUs( _NRF24L01P_TIMING_Tpd2stby_us );
    }

    void SetReceiveMode()
    {
        if ( ChipMode::POWER_DOWN == mMode ) SetPowerUp(true);
        uint8_t theConfig = GetRegister(_NRF24L01P_REG_CONFIG);
        theConfig |= _NRF24L01P_CONFIG_PRIM_RX;
        SetRegister(_NRF24L01P_REG_CONFIG, theConfig);
        mMode = ChipMode::RX;
    }

    void SetTransmitMode()
    {
        if ( ChipMode::POWER_DOWN == mMode ) SetPowerUp(true);
        uint8_t theConfig = GetRegister(_NRF24L01P_REG_CONFIG);
        theConfig &= ~_NRF24L01P_CONFIG_PRIM_RX;
        SetRegister(_NRF24L01P_REG_CONFIG, theConfig);
        mMode = ChipMode::TX;
    }

    void SetEnable(bool inEnable)
    {
        mEnabled = inEnable;
        if ( mEnabled )
        {
            NRF24L01P_RaiseCE();
            NRF2401LP_DelayUs(_NRF24L01P_TIMING_Tpece2csn_us );
        }
        else
        {
            NRF24L01P_DropCE();
        }
    }

    unsigned char GetRegister( unsigned char inReg ) {
        NRF24L01P_DropCSN();
        mSPI->writeRead(inReg);
        unsigned char data = mSPI->writeRead(_NRF24L01P_SPI_CMD_NOP);
        NRF24L01P_RaiseCSN();
        return data;
    }

    void SetRegister( unsigned char inReg, unsigned char inRegData ) {
        volatile unsigned char cn = (_NRF24L01P_SPI_CMD_WR_REG | inReg);
        NRF24L01P_DropCSN();
        mSPI->writeRead(cn);
        mSPI->writeRead(inRegData);
        NRF24L01P_RaiseCSN();
        NRF2401LP_DelayUs(_NRF24L01P_TIMING_Tpece2csn_us );
    }

    int GetStatusRegister()
    {
        NRF24L01P_DropCSN();
        unsigned char status = mSPI->writeRead( _NRF24L01P_SPI_CMD_NOP );
        NRF24L01P_RaiseCSN();
        return status;
    }

    int Write(unsigned char *data, int count)
    {
        NRF24L01P_DropCE();
        if ( count > _NRF24L01P_TX_FIFO_SIZE )
            count = _NRF24L01P_TX_FIFO_SIZE;

        // Clear the Status bit
        SetRegister(_NRF24L01P_REG_STATUS, _NRF24L01P_STATUS_TX_DS);
        NRF24L01P_DropCSN();
        mSPI->writeRead(_NRF24L01P_SPI_CMD_WR_TX_PAYLOAD);
        for ( int i = 0; i < count; i++ )
            mSPI->writeRead(*data++);
        NRF24L01P_RaiseCSN();

        ChipMode originalMode = mMode;
        SetTransmitMode();

        NRF24L01P_RaiseCE();
        NRF2401LP_DelayUs(_NRF24L01P_TIMING_Thce_us);
        NRF24L01P_DropCE();

        while ( !( GetStatusRegister() & _NRF24L01P_STATUS_TX_DS ) );

        // Clear the Status bit
        SetRegister(_NRF24L01P_REG_STATUS, _NRF24L01P_STATUS_TX_DS);
        if ( originalMode == ChipMode::RX )
            SetReceiveMode();

        SetEnable( mEnabled );
        return count;
    }

    int Read( unsigned char *data, int count)
    {
        if ( count <= 0 ) return 0;
        if ( count > _NRF24L01P_RX_FIFO_SIZE ) count = _NRF24L01P_RX_FIFO_SIZE;
        if ( Readable())
        {
            NRF24L01P_DropCSN();
            mSPI->writeRead(_NRF24L01P_SPI_CMD_R_RX_PL_WID);
            int rxPayloadWidth = mSPI->writeRead(_NRF24L01P_SPI_CMD_NOP);
            NRF24L01P_RaiseCSN();

            if ( ( rxPayloadWidth < 0 ) || ( rxPayloadWidth > _NRF24L01P_RX_FIFO_SIZE ) )
            {
                NRF24L01P_DropCSN();
                mSPI->writeRead(_NRF24L01P_SPI_CMD_FLUSH_RX);
                mSPI->writeRead(_NRF24L01P_SPI_CMD_NOP);
                NRF24L01P_RaiseCSN();

            }
            else
            {
                if ( rxPayloadWidth < count ) count = rxPayloadWidth;
                NRF24L01P_DropCSN();
                mSPI->writeRead(_NRF24L01P_SPI_CMD_RD_RX_PAYLOAD);
                for ( int i = 0; i < count; i++ )
                {
                    *data++ = mSPI->writeRead(_NRF24L01P_SPI_CMD_NOP);
                }
                NRF24L01P_RaiseCSN();
                // Clear the Status bit
                SetRegister(_NRF24L01P_REG_STATUS, _NRF24L01P_STATUS_RX_DR);
                return count;
            }
        }
        return -1;
    }

    bool Readable()
    {
        uint8_t theStatus = GetStatusRegister();
        return ( ( theStatus & _NRF24L01P_STATUS_RX_DR ) && ( ( ( theStatus & _NRF24L01P_STATUS_RX_P_NO ) >> 1 ) == 0 ) );
    }


private:
    ChipMode mMode;
    bool mEnabled;
    writeReadInterface* mSPI;
};

#endif // NRF24L01P_H
