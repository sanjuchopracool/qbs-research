#ifndef NRF24L01_CONFIG_H
#define NRF24L01_CONFIG_H

// These macros should be specific to microcontroller
extern void
_delay_us(double __us);
// Enable NRF24L01P as slave
#define NRF24L01P_EnableSlave()  PORTB &= 0b11111011
// Disable NRF24L01P as slave
#define NRF24L01P_DisableSlave() PORTB |= 0b00000100

// Enable transfer for NRF24L01P
#define NRF24L01P_EnableTransfer() PORTB |= 0b00000010
// Disable transfer for  NRF24L01P
#define NRF24L01P_DisableTransfer()  PORTB &= 0b11111101

// Function to delay in microseconds
#define NRF2401LP_DelayUs( x ) _delay_us( x)
#endif // NRF24L01_CONFIG_H
