#include <avr/io.h>

#define F_CPU 20000000
#include <util/delay.h>

#include "nrf2401lp_device.h"

void UART_init()
{
    // F_CPU 16000000
    // UBRR 103
    UBRR0L = 103;  // set prescaler for baud rate
    UCSR0B |= 0x08; // enable transmitter
}

void UART_Transmit( unsigned char data )
{
    while ( ! ( UCSR0A &  ( 1 << UDRE0) ) );
    UDR0 = data;
}

void printInt( int data )
{
    char buf[5]; // long enough for largest number
    signed char counter = 0;

    if ( data < 0 )
    {
        data *= -1;
        UART_Transmit( '-' );
    }

     if (data == 0)
       buf[counter++] = '0';

     for ( ; data; data /= 10)
     {
         buf[counter++] = '0' + data%10;
     }

     counter --;
     while ( counter >= 0)
         UART_Transmit(buf[counter--]);
}

class SPI
{
public:
    SPI() {
        // SPI running at F_CPU/2 ( 8MHz)
        DDRB |= (1<<5)|(1<<3);
        SPCR = (1<<SPE)|(1<<MSTR);
        SPSR |= ( 1 << SPI2X);
    }

    unsigned char writeRead( unsigned char data ) {
        SPDR = data;
        while(!(SPSR & (1<<SPIF) ));
        return(SPDR);
    }

    SPI( const SPI& other) = delete;
    SPI& operator =( const SPI& other) = delete;
    SPI& operator =( const SPI&& other) = delete;
    SPI( const SPI&& other) = delete;
};

int main()
{
    UART_init();
    DDRB |= (1<<2) | ( 1 << 1);
    SPI spi;
    NRF24L01P<SPI> theRf(&spi);
    UART_Transmit('*');
    printInt( theRf.GetRegister(0));
    UART_Transmit('\n');
    printInt( theRf.GetRegister(1));
    UART_Transmit('\n');
    printInt( theRf.GetRegister(2));
    UART_Transmit('\n');
    printInt( theRf.GetRegister(3));
    theRf.SetPowerUp( true );
    theRf.SetEnable( true );
    theRf.SetTransmitMode();
    unsigned char data[10] = "Hello!!!";
    while ( 1 )
    {
        for( int i = 0; i < 250; i++ )
        {
           _delay_ms(1);
        }
        theRf.Write(data, 10);
//        UART_Transmit('*');
//        if ( theRf.Readable() )
//        {
//            theRf.read( data, 10 );
//            for ( int i = 0; i < 10; ++i)
//                UART_Transmit( data[i] );
//            UART_Transmit('\n');
//        }
    }
}
